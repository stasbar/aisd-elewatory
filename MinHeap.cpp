//
// Created by Admin1 on 08.05.2016.
//


#include "MinHeap.h"

using namespace std;

MinHeap::MinHeap(vector<Elewator *> *_elewators) {
    elewators = _elewators;
    Heapify();
}

void MinHeap::Heapify() {
    cout << "Heapify" << endl;
    int length = elewators->size();
    for (int i = length - 1; i >= 0; --i) {
        BubbleDown(i);
    }
}

void MinHeap::BubbleDown(int index) {
    print();
    cout << "BubbleDown index " << index << endl;
    int length = elewators->size();
    cout << "BubbleDown lenth " << length << endl;
    int leftChildIndex = 2 * index + 1;
    int rightChildIndex = 2 * index + 2;

    if (leftChildIndex >= length)
        return; //index is a leaf

    int minIndex = index;

    if ((elewators->at(index)->getMasa() > elewators->at(leftChildIndex)->getMasa()) ||
        ((elewators->at(index)->getMasa() == elewators->at(leftChildIndex)->getMasa()) &&
         (elewators->at(index)->getNr() > elewators->at(leftChildIndex)->getNr()))) {
        cout << "minIndex = leftChildIndex" << endl;
        minIndex = leftChildIndex;
    }

    if ((rightChildIndex < length) &&
        ((elewators->at(minIndex)->getMasa() > elewators->at(rightChildIndex)->getMasa()) ||
         ((elewators->at(minIndex)->getMasa() == elewators->at(rightChildIndex)->getMasa()) &&
          (elewators->at(minIndex)->getNr() > elewators->at(rightChildIndex)->getNr())))) {
        cout << "minIndex = rightChildIndex;" << endl;
        minIndex = rightChildIndex;
    }

    if (minIndex != index) {

        //need to swap
        Elewator *temp = elewators->at(index);
        elewators->at(index) = elewators->at(minIndex);
        elewators->at(minIndex) = temp;
        BubbleDown(minIndex);
    }
}

void MinHeap::BubbleUp(int index) {
    if (index == 0)
        return;

    int parentIndex = (index - 1) / 2;

    if (elewators->at(parentIndex)->getMasa() > elewators->at(index)->getMasa() ||
        ((elewators->at(parentIndex)->getMasa() == elewators->at(index)->getMasa()) &&
         (elewators->at(parentIndex)->getNr() > elewators->at(index)->getNr()))) {
        Elewator *temp = elewators->at(parentIndex);
        elewators->at(parentIndex) = elewators->at(index);
        elewators->at(index) = temp;
        BubbleUp(parentIndex);
    }
}

Elewator *MinHeap::GetMin() {
    return elewators->at(0);
}

void MinHeap::DeleteMin() {


    unsigned long length = elewators->size();

    if (length == 0) {
        return;
    }

    elewators->at(0) = elewators->at(length - 1);
    elewators->pop_back();

    BubbleDown(0);

}

void MinHeap::print() {
    cout << "Print" << endl;
    for (int i = 0; i < elewators->size(); i++)
        cout << elewators->at(i)->getMasa() << " ";
}

void MinHeap::printElewator(int nr) {
    for (int i = 0; i < elewators->size(); i++) {
        if (elewators->at(i)->getNr() == nr)
            elewators->at(i)->print();
    }
}


void MinHeap::napelnij(int nr, int masa) {
    if(masa==0)
        return;
    //Find index of nr
    int index = -1;
    int aktualnaMasa = -1;

    for (int i = 0; i < elewators->size(); i++) {
        if (elewators->at(i)->getNr() == nr) {
            index = i;
            aktualnaMasa = elewators->at(i)->getMasa();
        }
    }
    if (index < 0 || aktualnaMasa < 0 || aktualnaMasa == masa)
        return;
    elewators->at(index)->setMasa(masa);
    if (aktualnaMasa < masa)
        BubbleDown(index);
    else
        BubbleUp(index);


}

void MinHeap::rozladuj(int nr, int masa) {
//Find index of nr
    int index = -1;

    for (int i = 0; i < elewators->size(); i++) {
        if (elewators->at(i)->getNr() == nr)
            index = i;
    }
    if (index < 0 || masa == 0)
        return;

    elewators->at(index)->oddMasa(masa);
    BubbleUp(index);

}

void MinHeap::naladujNajmniejszy(int masa){

}
int MinHeap::findNajmniejszy(int index){
    int masaIndex = elewators->at(index)->getMasa();
    if(masaIndex==0) {
        int lewaIndex = findNajmniejszy(index*2+1);
        int prawa = findNajmniejszy(index*2+2);
        if(lewa==0 && prawa==0)
            return -1;
        if(lewa > prawa) {
            elewators->at(index * 2 + 1)->addMasa(masa);
            return -1;
        }else if(prawa > lewa){
            elewators->at(index * 2 + 2)->addMasa(masa);
            return -1;
        }else if(lewa == prawa){
            if(elewators->at(index * 2 + 1)->getNr() > elewators->at(index * 2 + 2)->getNr()){
                elewators->at(index * 2 + 2)->addMasa(masa);
            }else{
                elewators->at(index * 2 + 1)->addMasa(masa);
            }

        }
    }
    else return index;
}

int MinHeap::findIndex(int nr) {
    return 0;
}






















