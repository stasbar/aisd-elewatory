#include <iostream>
#include <vector>

#include "MinHeap.h"
#include "MaxHeap.h"

class ElewatorManager;

using namespace std;

MinHeap *minHeap;
MaxHeap *maxHeap;
vector<Elewator *> *elewators;

void execute(string command){
    cout << "wykonuje akcje " << command << endl;
    if (command.compare("n") == 0) {
        int nr;
        cin >> nr;
        int masa;
        cin >> masa;
        minHeap->
        minHeap->napelnij(nr, masa);
    } else if (command.compare("nm") == 0) {
        int nr;
        cin >> nr;
        int masa;
        cin >> masa;
        rozladuj(nr, masa);

    } else if (command.compare("nM") == 0) {

    } else if (command.compare("r") == 0) {

    } else if (command.compare("rm") == 0) {

    } else if (command.compare("rM") == 0) {

    } else if (command.compare("w") == 0) {
        int nr;
        cin >> nr;

        printElewator(nr);


    } else if (command.compare("wm") == 0) {

    } else if (command.compare("wM") == 0) {

    }
}
int main() {


    cout << "Enter ilosc Elewatorów" << endl;
    int iloscElewatorow;
    cin >> iloscElewatorow;

    elewators = new vector<Elewator *>(iloscElewatorow);


    cout << "Enter ilosc operacji" << endl;
    int iloscOperacji;
    cin >> iloscOperacji;

    for (int i = 0; i < iloscElewatorow; i++) {
        int masa;
        cout << "Enter masa dla: " << i << endl;
        cin >> masa;
        elewators->at(i) = new Elewator(i, masa);
    }

    minHeap= new MinHeap(elewators);
    maxHeap= new MaxHeap(elewators);
    for (int i = 0; i < iloscOperacji; i++) {
        string akcja;
        cin >> akcja;
        execute(akcja);
    }

    return 0;
}