//
// Created by Admin1 on 08.05.2016.
//

#ifndef ELEWATORY_MAXHEAP_H
#define ELEWATORY_MAXHEAP_H


#include <stdio.h>
#include <vector>
#include <iostream>
#include "Elewator.h"
using namespace std;
class MaxHeap{
public:
    vector<Elewator*> * elewators;
    void BubbleDown(int index);
    void BubbleUp(int index);
    void Heapify();
    void printElewator(int nr);
    void napelnij(int nr, int masa);
    void rozladuj(int nr, int masa);
    void naladujNajwiekszy(int index, int masa);

    MaxHeap(vector<Elewator*> * _elewators);
    Elewator* GetMax();
    void DeleteMax();
    void print();
};



#endif //ELEWATORY_MAXHEAP_H
