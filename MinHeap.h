//
// Created by Admin1 on 08.05.2016.
//

#ifndef ELEWATORY_MANAGER_H
#define ELEWATORY_MANAGER_H


#include <stdio.h>
#include <vector>
#include <iostream>
#include "Elewator.h"
using namespace std;
class MinHeap{
public:
    vector<Elewator*> * elewators;
    vector<int> * heapIndexs;
    void BubbleDown(int index);
    void BubbleUp(int index);
    void Heapify();
    void printElewator(int nr);
    void napelnij(int nr, int masa);
    void rozladuj(int nr, int masa);
    void naladujNajmniejszy(int masa);
    int findNajmniejszy(int index);
    int findIndex(int nr);

    MinHeap(vector<Elewator*> * _elewators);
    Elewator* GetMin();
    void DeleteMin();
    void print();
};


#endif //ELEWATORY_MANAGER_H
