//
// Created by Admin1 on 08.05.2016.
//

#ifndef ELEWATORY_ELEWATOR_H
#define ELEWATORY_ELEWATOR_H


#include <stdio.h>
#include <iostream>

class Elewator{
    int nr;
    int masa;
    int indexMax;
    int indexMin;
public:
    Elewator(int masa, int nrElewatora);
    int getMasa();
    void setMasa(int _masa);
    int getNr();
    void setNr(int _nr);
    void oddMasa(int masa);
    void addMasa(int masa);
    void print();
    void setIndexMax(int index){
        indexMax = index;
    }
    int getIndexMax(){
        return indexMax;
    }
    void setIndexMin(int index){
        indexMin = index;
    }
    int getIndexMin(){
        return indexMin;
    }
};


#endif //ELEWATORY_ELEWATOR_H
